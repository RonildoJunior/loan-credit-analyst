import axios from 'axios';

export function fetchLoanApplication(){
	return function(dispatch){
		axios.get('http://localhost:8000/loan_applications/')
			.then((response) =>{
			dispatch({type: 'FETCH_LOAN_APPLICATION_FULFILLED', payload:response.data})
		})
		.catch((err) => {
			dispatch({type: 'FETCH_LOAN_APPLICATION_REJECTED', payload:err})
		})
	}
}