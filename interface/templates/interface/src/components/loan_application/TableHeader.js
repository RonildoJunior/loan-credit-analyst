import React from "react";

export default class LoanAppTableHeader extends React.Component {

    render() {
        return (
            <thead>
                <tr>
                    <td>ID</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Amount</td>
                    <td>Status</td>
                    <td>Date</td>
                    <td>  </td>
                    <td>  </td>
                </tr>
            </thead>
        )
    }
}
