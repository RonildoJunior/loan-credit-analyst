import React from "react";

export default class SearchBar extends React.Component {

    render() {
        return (
            <form onSubmit={this.searchLoanApplication.bind(this)}>
                <input type="text" ref="lats_first_name" placeholder="First Name"/>
                <input type="text" ref="lats_last_name" placeholder="Last Name"/>
                <input type="text" ref="lats_amount" placeholder="Amount"/>
                <input type="text" ref="lats_status" placeholder="Status"/>
                <input type="text" ref="lats_date" placeholder="Date"/>
                <button type="submit" >Search</button>
            </form>
        );
    }

    searchLoanApplication(e){
        e.preventDefault();
        var filter = {
            first_name: this.refs.lats_first_name.value,
            last_name: this.refs.lats_last_name.value,
            amount: this.refs.lats_amount.value,
            status: this.refs.lats_status.value,
            date: this.refs.lats_date.value
        }
        this.props.onSearchLoanApplication(filter);
    }
}
