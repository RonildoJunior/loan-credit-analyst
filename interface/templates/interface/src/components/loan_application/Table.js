import React from "react";
import {connect} from "react-redux";
import TableHeader from "./TableHeader.js";
import TableRow from "./TableRow.js";
import SearchBar from "./TableSearchBar.js";
import {fetchLoanApplication} from "../../actions/loanApplicationActions";

@connect((store) =>{
    return {
        loanApplication: store.loanApplicationReducer.loanApplication
    }
})

export default class Table extends React.Component {

    componentWillMount() {
        this.props.dispatch(fetchLoanApplication());
    }

    render() {
        const {loanApplication} = this.props;
        var didLoad = loanApplication != null ? true : false;

        return (
            <div className="tableLoanApplication">
                <SearchBar onSearchLoanApplication = {this.onSearchLoanApplication.bind(this)}/>
                <table>
                    <TableHeader />
                    <tbody>
                        { didLoad ? loanApplication.results.map(function (result, index) {
                            return <TableRow key={index} data={result}/>;
                        }) : <tr><td colSpan="6">No data</td></tr>}
                    </tbody>
                </table>
            </div>
        );
    }

    onSearchLoanApplication(filter){
        this.props.dispatch(fetchLoanApplication());
    }
}