import React from "react";

export default class TableRow extends React.Component {

    render() {
        return (
            <tr>
                <td>{this.props.data.pk}</td>
                <td>{this.props.data.client.first_name}</td>
                <td>{this.props.data.client.last_name}</td>
                <td>{this.props.data.loan_amount}</td>
                <td>{this.props.data.loan_status}</td>
                <td>{this.props.data.application_date}</td>
                <td> <a href="edit" onClick={this.loadLoanApplicationDetails.bind(this)} > Edit </a> </td>
                <td> <a href="delete" > Delete </a> </td>
            </tr>
        )
    }

    loadLoanApplicationDetails(e){
        e.preventDefault();
        console.log("editing a loan application");
    }
}
