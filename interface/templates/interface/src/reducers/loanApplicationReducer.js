const initialState = {
	loanApplication: null,
    fetching: false,
	fetched: false,
	error: null
}

export default function reducer(state=initialState, action){
	switch(action.type){
		case "FETCH_LOAN_APPLICATION":{
			return {...state, fetching: true, action}
		}
		case "FETCH_LOAN_APPLICATION_REJECTED":{
			return {...state, fetching: false, error: action.payload}
		}
		case "FETCH_LOAN_APPLICATION_FULFILLED":{
			return {...state, fetching: false, fetched:true, loanApplication:action.payload}
		}
	}
	return state;
}