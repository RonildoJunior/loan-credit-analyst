import {combineReducers} from "redux"

import loanApplicationReducer from "./loanApplicationReducer"

export default combineReducers({
	loanApplicationReducer
});