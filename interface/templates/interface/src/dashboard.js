import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import LoanApplicationTable from "./components/loan_application/Table.js";
import LoanApplicationDetails from "./components/loan_application/Details.js";
import store from "./store";

ReactDOM.render(
    <Provider store={store}>
        <LoanApplicationTable />
    </Provider>, document.getElementById('loan_application_table'));

ReactDOM.render(<LoanApplicationDetails />, document.getElementById('loan_application_details'));