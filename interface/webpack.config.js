module.exports = {
     entry: './templates/interface/src/dashboard.js',
     output: {
         path: './static/js/compiled/',
         filename: 'dashboard.js',
     },
     module: {
         loaders: [{
             test: /\.jsx?$/,
             exclude: /node_modules/,
             loader: 'babel-loader',
             query: {
                presets: ['react', 'es2015', 'stage-0'],
                plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy'],
             }
         }]
     }
 }