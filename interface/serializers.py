from rest_framework import serializers

from .models import *

class AddressSerializer(serializers.ModelSerializer):
    streetName = serializers.CharField(source='street_name')
    streetNumber = serializers.IntegerField(source='street_number')
    startDate = serializers.DateTimeField(source='start_date')
    endDate = serializers.DateTimeField(source='end_date')

    class Meta:
        model = Address
        exclude = ('id', 'street_name', 'street_number', 'start_date', 'end_date')


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Person


class BankDetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BankDetails


class SMSFundAssetsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SMSFundAssets


class LoanFinancialDetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LoanFinancialDetails


class BeneficiaryLiabilitiesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BeneficiaryLiabilities


class BeneficiaryAssetsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BeneficiaryAssets


class BeneficiaryEmployersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BeneficiaryEmployers


class PropertyRatingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PropertyRating


class PropertySerializer(serializers.HyperlinkedModelSerializer):
    property_rating_list = serializers.HyperlinkedRelatedField(many=True, view_name='propertyrating-detail', read_only=True)

    class Meta:
        model = Property


class LoanApplicationSerializer(serializers.HyperlinkedModelSerializer):
    loan_financial_details_list = serializers.HyperlinkedRelatedField(many=True, view_name='loanfinancialdetails-detail', read_only=True)
    property = serializers.HyperlinkedRelatedField(view_name='property-detail', read_only=True)

    class Meta:
        model = LoanApplication


class BeneficiarySerializer(serializers.HyperlinkedModelSerializer):
    assets_list = serializers.HyperlinkedRelatedField(many=True,view_name='beneficiaryassets-detail', read_only=True)
    employers_list = serializers.HyperlinkedRelatedField(many=True, view_name='beneficiaryemployers-detail', read_only=True)
    liabilities_list = serializers.HyperlinkedRelatedField(many=True, view_name='beneficiaryliabilities-detail', read_only=True)

    class Meta:
        model = Beneficiary


class CorporateTrusteeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateTrustee


class PropertyGuaranteeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PropertyGuarantee


class PropertyTrusteeSerializer(serializers.HyperlinkedModelSerializer):
    loan_applications_list = serializers.HyperlinkedRelatedField(many=True, view_name='loanapplication-detail', read_only=True)

    class Meta:
        model = PropertyTrustee


class SMSFundSerializer(serializers.HyperlinkedModelSerializer):
    property_trustee = serializers.HyperlinkedRelatedField(view_name='propertytrustee-detail', read_only=True)
    corporate_trustee = serializers.HyperlinkedRelatedField(view_name='corporatetrustee-detail', read_only=True)
    property_guarantee = serializers.HyperlinkedRelatedField(view_name='propertyguarantee-detail', read_only=True)
    bank_details = serializers.HyperlinkedRelatedField(view_name='bankdetails-detail', read_only=True)
    beneficiaries_list = serializers.HyperlinkedRelatedField(many=True, view_name='beneficiary-detail', read_only=True)
    loan_applications_list = serializers.HyperlinkedRelatedField(many=True, view_name='loanapplication-detail', read_only=True)

    class Meta:
        model = SMSFund

