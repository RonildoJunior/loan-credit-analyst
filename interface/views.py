from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from rest_framework import viewsets

from interface.forms import LoginForm
from interface.serializers import *
from interface.services import LoanApplicationService


class AddressViewSet(viewsets.ModelViewSet):
    serializer_class = AddressSerializer

    def get_queryset(self):
        return LoanApplicationService().list_address(self.request.user.id)


class SMSFundViewSet(viewsets.ModelViewSet):
    serializer_class = SMSFundSerializer

    def get_queryset(self):
        return LoanApplicationService().list_smsfund(self.request.user.id)


class CorporateTrusteeViewSet(viewsets.ModelViewSet):
    serializer_class = CorporateTrusteeSerializer

    def get_queryset(self):
        return LoanApplicationService().list_corporate_trustee(self.request.user.id)


class PropertyTrusteeViewSet(viewsets.ModelViewSet):
    serializer_class = PropertyTrusteeSerializer
    queryset = PropertyTrustee.objects.all()


class PropertyGuaranteeViewSet(viewsets.ModelViewSet):
    serializer_class = PropertyGuaranteeSerializer

    def get_queryset(self):
        return LoanApplicationService().list_property_guarantee(self.request.user.id)


class BankDetailsViewSet(viewsets.ModelViewSet):
    serializer_class = BankDetailsSerializer

    def get_queryset(self):
        return LoanApplicationService().list_bank_details(self.request.user.id)


class SMSFundAssetsViewSet(viewsets.ModelViewSet):
    serializer_class = SMSFundAssetsSerializer

    def get_queryset(self):
        return LoanApplicationService().list_smsfund_assets(self.request.user.id)


class BeneficiaryViewSet(viewsets.ModelViewSet):
    serializer_class = BeneficiarySerializer

    def get_queryset(self):
        return LoanApplicationService().list_beneficiaries(self.request.user.id)


class LoanApplicationViewSet(viewsets.ModelViewSet):
    serializer_class = LoanApplicationSerializer

    def get_queryset(self):
        return LoanApplicationService().list_loan_application(self.request.user.id)


class LoanFinancialDetailsViewSet(viewsets.ModelViewSet):
    serializer_class = LoanFinancialDetailsSerializer

    def get_queryset(self):
        return LoanApplicationService().list_loan_financial_details(self.request.user.id)


class PropertyViewSet(viewsets.ModelViewSet):
    serializer_class = PropertySerializer

    def get_queryset(self):
        return LoanApplicationService().list_property(self.request.user.id)


class PropertyRatingViewSet(viewsets.ModelViewSet):
    serializer_class = PropertyRatingSerializer

    def get_queryset(self):
        return LoanApplicationService().list_property_rating(self.request.user.id)


class BeneficiaryLiabilitiesViewSet(viewsets.ModelViewSet):
    serializer_class = BeneficiaryLiabilitiesSerializer

    def get_queryset(self):
        return LoanApplicationService().list_beneficiary_liabilities(self.request.user.id)


class BeneficiaryAssetsViewSet(viewsets.ModelViewSet):
    serializer_class = BeneficiaryAssetsSerializer

    def get_queryset(self):
        return LoanApplicationService().list_beneficiary_assets(self.request.user.id)


class BeneficiaryEmployersViewSet(viewsets.ModelViewSet):
    serializer_class = BeneficiaryEmployersSerializer

    def get_queryset(self):
        return LoanApplicationService().list_beneficiary_employers(self.request.user.id)


def login(request):
    return render(request, 'interface/login.html', {'form': LoginForm()})


def logout(request):
    auth.logout(request)
    return render(request, 'interface/login.html', {'form': LoginForm()})


def verify_login(request):
    form = LoginForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('dashboard')
    return redirect('login')


@login_required(login_url='/login/')
def dashboard(request):
    return render(request, 'interface/dashboard.html', {})
