import uuid

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.validators import RegexValidator
from django.db import models

# messages
message_phone_regex = "Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=message_phone_regex)

DAY = 'D'
MONTH = 'M'
YEAR = 'Y'

MARRIED = 'MA'
WIDOWED = 'WI'
SEPARATED = 'SE'
DIVORCED = 'DI'
SINGLE = 'SI'
MASTER = 'Master'

MR = 'Mr'
MISS = 'Mi'
MS = 'Ms'
MRS = 'Mrs'
MX = 'Mx'


class Address(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    street_name = models.CharField(max_length=50)
    street_number = models.IntegerField()
    suburb = models.CharField(max_length=50)
    zipcode = models.IntegerField()
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)


class Person(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()
    mobile_phone = models.CharField(validators=[phone_regex], blank=True, max_length=15)
    home_phone = models.CharField(validators=[phone_regex], blank=True,max_length=15)
    dbo = models.DateTimeField(blank=True, null=True)
    marital_status = ((MARRIED, 'Married'), (WIDOWED, 'Widowed'), (SEPARATED, 'Separated'), (DIVORCED, 'Divorced'), (SINGLE, 'Single'),)
    formal_titles = ((MR, 'Mr'), (MISS, 'Miss'), (MS, 'Ms'), (MRS, 'Mrs'), (MX, 'Mx'),)
    number_of_dependents = models.IntegerField()
    driver_license = models.CharField(max_length=30)
    mother_maiden_name = models.CharField(max_length=30)
    address = GenericRelation(Address)


class SMSFund(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    fund_name = models.CharField(max_length=30, null=True)
    abn = models.IntegerField()
    fund_incorporation_date = models.DateTimeField(blank=True, null=True)
    amount = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    address = GenericRelation(Address)


class CorporateTrustee(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    fund = models.OneToOneField(SMSFund, related_name='corporate_trustee', on_delete=models.CASCADE)
    acn = models.IntegerField()
    name = models.CharField(max_length=30)
    ct_incorporation_date = models.DateTimeField(blank=True, null=True)
    type_of_company = models.CharField(max_length=10)
    address = GenericRelation(Address)


class PropertyTrustee(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    fund = models.OneToOneField(SMSFund, related_name='property_trustee', on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    acn = models.IntegerField(null=True)
    abn = models.IntegerField(null=True)
    pt_incorporation_date = models.DateTimeField(blank=True, null=True)
    address = GenericRelation(Address)


class PropertyGuarantee(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30)
    fund = models.OneToOneField(SMSFund, related_name='property_guarantee', on_delete=models.CASCADE)
    address = GenericRelation(Address)


class BankDetails(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    fund = models.OneToOneField(SMSFund, related_name='bank_details', on_delete=models.CASCADE)
    name = models.CharField(max_length=30, null=True)
    bank_number = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    account_number = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    amount = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    address = GenericRelation(Address)


class SMSFundAssets(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    fund = models.ForeignKey(SMSFund, related_name='smsfund_assets_list', on_delete=models.CASCADE)
    name = models.CharField(max_length=10)
    description = models.CharField(max_length=100)
    value = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    address = GenericRelation(Address)


class Beneficiary(Person):
    person = models.OneToOneField(Person, related_name='beneficiary', on_delete=models.CASCADE, default='0000000', editable=False)
    smsfund = models.ForeignKey(SMSFund, related_name='beneficiaries_list', on_delete=models.CASCADE)
    corporate_trustee = models.ForeignKey(CorporateTrustee, related_name='beneficiaries_list', on_delete=models.CASCADE)
    property_trustee = models.ForeignKey(PropertyTrustee, related_name='beneficiaries_list', on_delete=models.CASCADE, null=True)
    property_guarantee = models.ForeignKey(PropertyGuarantee, related_name='beneficiaries_list', on_delete=models.CASCADE, null=True)


class LoanApplication(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    smsfund = models.ForeignKey(SMSFund, related_name='loan_applications_list', on_delete=models.CASCADE)
    property_trustee = models.ForeignKey(PropertyTrustee, related_name='loan_applications_list', on_delete=models.CASCADE)
    application_date = models.DateTimeField(blank=True)
    loan_status = models.CharField(max_length=10)
    loan_purpose = models.CharField(max_length=10, null=True)
    loan_original_term = models.CharField(max_length=10, null=True)


class LoanFinancialDetails(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    loan_application = models.ForeignKey(LoanApplication, related_name='loan_financial_details_list', on_delete=models.CASCADE)
    interest_rate = models.DecimalField(max_digits=4, decimal_places=2)
    interest_type = models.CharField(max_length=10, null=True)
    margin = models.DecimalField(max_digits=4, decimal_places=2)
    rate_reset = models.IntegerField()
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    fixed_rate = models.DecimalField(max_digits=4, decimal_places=2)
    loan_term = models.CharField(max_length=10, null=True)
    risk_grade = models.DecimalField(max_digits=4, decimal_places=2)
    frequency = models.CharField(max_length=10, null=True)
    portfolio = models.CharField(max_length=10, null=True)
    repayment_type = models.CharField(max_length=10, null=True)
    loan_term = models.CharField(max_length=10, null=True)
    date = models.DateTimeField(blank=True)


class Property(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    loan_application = models.OneToOneField(LoanApplication, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    title = models.CharField(max_length=10)
    insurance = models.CharField(max_length=10)
    property_usage = models.CharField(max_length=10)
    address = GenericRelation(Address)


class PropertyRating(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    property = models.ForeignKey(Property, related_name='property_rating_list', on_delete=models.CASCADE)
    rating_score = models.DecimalField(max_digits=3, decimal_places=2)
    property_evaluation = models.DecimalField(max_digits=10, decimal_places=2)
    rental_evaluation = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    evaluation_type = models.CharField(max_length=10)
    evaluation_source = models.CharField(max_length=10)


class BeneficiaryLiabilities(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    beneficiary = models.ForeignKey(Beneficiary, related_name='liabilities_list', on_delete=models.CASCADE)
    name = models.CharField(max_length=10)
    description = models.CharField(max_length=100)
    outcome = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    period = ((DAY, 'Day'), (MONTH, 'Month'), (YEAR, 'YEAR'))


class BeneficiaryAssets(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    beneficiary = models.ForeignKey(Beneficiary, related_name='assets_list', on_delete=models.CASCADE)
    name = models.CharField(max_length=10)
    description = models.CharField(max_length=100)
    income = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    period = ((DAY, 'Day'), (MONTH, 'Month'), (YEAR, 'YEAR'))
    address = GenericRelation(Address)


class BeneficiaryEmployers(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    beneficiary = models.ForeignKey(Beneficiary, related_name='employers_list', on_delete=models.CASCADE)
    company_name = models.CharField(max_length=10)
    supervisor_name = models.CharField(max_length=10)
    mobile_phone = models.CharField(validators=[phone_regex], blank=True, max_length=15)
    office_phone = models.CharField(validators=[phone_regex], blank=True, max_length=15)
    occupation = models.CharField(max_length=10)
    gross_salary = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    date_started = models.DateTimeField(blank=True, null=True)
    date_finished = models.DateTimeField(blank=True, null=True)
    address = GenericRelation(Address)
