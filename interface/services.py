from .models import *


class LoanApplicationService:

    def list_address(self, user_id):
        return Address.objects.all()

    def list_person(self, user_id):
        return Person.objects.all()

    def list_smsfund(self, user_id):
        return SMSFund.objects.all()

    def list_corporate_trustee(self, user_id):
        return CorporateTrustee.objects.all()

    def list_property_trustee(self, user_id):
        return PropertyTrustee.objects.all()

    def list_property_guarantee(self, user_id):
        return PropertyGuarantee.objects.all()

    def list_smsfund_assets(self, user_id):
        return SMSFundAssets.objects.all()

    def list_bank_details(self, user_id):
        return BankDetails.objects.all()

    def list_beneficiaries(self, user_id):
        return Beneficiary.objects.all()

    def list_loan_application(self, user_id):
        return LoanApplication.objects.all()

    def list_loan_financial_details(self, user_id):
        return LoanFinancialDetails.objects.all()

    def list_property(self, user_id):
        return Property.objects.all()

    def list_property_rating(self, user_id):
        return PropertyRating.objects.all()

    def list_beneficiary_liabilities(self, user_id):
        return BeneficiaryLiabilities.objects.all()

    def list_beneficiary_assets(self, user_id):
        return BeneficiaryAssets.objects.all()

    def list_beneficiary_employers(self, user_id):
        return BeneficiaryEmployers.objects.all()

