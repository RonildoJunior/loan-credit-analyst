from django.conf.urls import url, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'smsfund', views.SMSFundViewSet, base_name='smsfund')
router.register(r'smsfundassets', views.SMSFundAssetsViewSet, base_name='smsfundassets')
router.register(r'corporatetrustee', views.CorporateTrusteeViewSet, base_name='corporatetrustee')
router.register(r'propertytrustee', views.PropertyTrusteeViewSet, base_name='propertytrustee')
router.register(r'propertyguarantee', views.PropertyGuaranteeViewSet, base_name='propertyguarantee')
router.register(r'bankdetails', views.BankDetailsViewSet, base_name='bankdetails')
router.register(r'beneficiary', views.BeneficiaryViewSet, base_name='beneficiary')
router.register(r'loanapplication', views.LoanApplicationViewSet, base_name='loanapplication')
router.register(r'loanfinancialdetails', views.LoanFinancialDetailsViewSet, base_name='loanfinancialdetails')
router.register(r'property', views.PropertyViewSet, base_name='property')
router.register(r'propertyrating', views.PropertyRatingViewSet, base_name='propertyrating')
router.register(r'beneficiaryliabilities', views.BeneficiaryLiabilitiesViewSet, base_name='beneficiaryliabilities')
router.register(r'beneficiaryassets', views.BeneficiaryAssetsViewSet, base_name='beneficiaryassets')
router.register(r'beneficiaryemployers', views.BeneficiaryEmployersViewSet, base_name='beneficiaryemployers')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^verify_login', views.verify_login, name='verify_login'),
    url(r'^dashboard', views.dashboard, name='dashboard'),

]

